<?php


use WHMCS\Module\Addon\SmsGateway\Hooks;
// check other way to do it!

$hook = new Hooks;
$smarty = new Smarty;
add_hook('InvoiceCreated', 1, function ($vars) use ($hook, $smarty){


    $hook->smsInvoiceCreated($vars, $smarty);
});

