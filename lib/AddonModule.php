<?php

namespace WHMCS\Module\Addon\SmsGateway;

//Load Models

//Load Controllers
use WHMCS\Module\Addon\SmsGateway\Admin\AdminDispatcher;
use WHMCS\Module\Addon\Setting;
use Illuminate\Database\Capsule\Manager as Capsule;
use WHMCS\Module\Addon\SmsGateway\Models\SmsTemplate;
use WHMCS\Module\Addon\SmsGateway\Logger;

class AddonModule


{

    /*
Make sure to use only fields available! 
https://developers.whmcs.com/addon-modules/configuration/
$fields may contain more than element and as also may required
*/



    protected $nubmerElements = 5;

    private $hooks = array('InvoiceCreated', 'ClientAddedViaApi');

    public function getHooks(): array{
        return $this->hooks;
    }

    public function getConfig(): array
    {
        return [
            // Display name for your module
            'name' => 'Sms Gateway',
            // Description displayed within the admin interface
            'description' => 'This module provides sms gateway module for multibrand sms sender.',
            // Module author name
            'author' => 'Zonat S.A',
            // Default language
            'language' => 'english',
            // Version number
            'version' => '1.0',
            'fields' => [

                'AccountSid' => [
                    'FriendlyName' => 'AccountSid',
                    'Type' => 'text',
                    'Size' => '120',
                    'Default' => '',
                    'Description' => 'Account SID',
                ],

                'AuthToken' => [
                    'FriendlyName' => 'AuthToken',
                    'Type' => 'password',
                    'Size' => '120',
                    'Default' => '',
                    'Description' => 'AuthToken',
                ],
                'Default' => [
                    'FriendlyName' => 'Default',
                    'Type' => 'text',
                    'Size' => '120',
                    'Default' => '',
                    'Description' => 'Default Service Sid Twilio',
                ],
            ]
        ];
    }







    public function activeAddon(): array
    {
        try {
            if (!Capsule::schema()->hasTable('smsgateway_sms_template')) {
                Capsule::schema()
                    ->create(
                        'smsgateway_sms_template',
                        function ($table) {
                            /** @var \Illuminate\Database\Schema\Blueprint $table */
                            $table->increments('id');
                            $table->string('name', 32);
                            $table->string('content', 151); // $table->text('description');
                            $table->string('lang', 15);
                            $table->timestamps();
                        }
                    );
            }

            if (!Capsule::schema()->hasTable('smsgateway_sms_stats')) {
                Capsule::schema()
                    ->create(
                        'smsgateway_sms_stats',
                        function ($table) {
                            $table->increments('id');
                            $table->enum('type', ['client', 'hosting', 'domain', 'ticket', 'invoice', 'quote', 'announcement']);
                            $table->bigInteger('relid');  // here check if there be a large number in the future!
                            $table->string('note', 32); // check if the noe may containe  more char.
                            $table->timestamps();
                        }
                    );
            }

            if (!Capsule::schema()->hasTable('smsgateway_services')) {
                Capsule::schema()
                    ->create(
                        'smsgateway_services',
                        function ($table) {
                            $table->increments('id');
                            $table->string('brandname', 32);
                            $table->string('sid', 80);
                            $table->timestamps();
                        }
                    );
            }
        } catch (\Exception $e) {
            return [
                'status' => "error",
                'description' => 'Unable to smsgateway_sms_template or smsgateway_sms_template: ' . $e->getMessage(),
            ];
        }


        try{
            $this->firstInstallationAddon($this->getHooks());
        }catch(\Exception $e){
            return [
                'status' => "error",
                'description' => 'Unable to generate tempaltes. error message: ' . $e->getMessage(),
            ];
        }


        return [
            'status' => 'success',
            'description' => 'Addon Sms Gateway creation init: success'
        ];
    }


    public function deactiveAddon(): void
    {
    }


    public function upgradeAddon($vars): void
    {
        $currentlyInstalledVersion = $vars['version'];
        Logger::logModule(  $currentlyInstalledVersion);
        if($currentlyInstalledVersion >= 1.0){
        try {
            if (!Capsule::schema()->hasTable('smsgateway_services')) {
                Capsule::schema()
                    ->create(
                        'smsgateway_services',
                        function ($table) {
                            $table->increments('id');
                            $table->string('brandname', 32);
                            $table->string('sid', 80);
                            $table->timestamps();
                        }
                    );
            }
        } catch (\Exception $e) {
    
            Logger::logModule('erron, Unable to smsgateway_sms_template, smsgateway_sms_template, smsgateway_services: ' . $e->getMessage());
        }

        //Run auto creating templates if not exist for hook.
        //Create InvoiceCreated templates.
        $this->createTemplateSchemaForHook($this->getHooks()[0]);
    }

    if($currentlyInstalledVersion > 1.1){
        //Create AddedViaApi templates.
        $this->createTemplateSchemaForHook($this->getHooks()[1]); 
    }


        Logger::logModule('success, Upgrade of Sms Module to version: ' . $currentlyInstalledVersion);
        
    }


    public function output($vars, $smarty)
    {
        $controller = isset($_REQUEST['controller']) ? $_REQUEST['controller'] : '';
        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

        $dispatcher = new AdminDispatcher();
        $response = $dispatcher->dispatch($controller, $action, $vars, $smarty);

        echo $response;
    }


    private function createTemplateSchemaForHook(string $hookName): void
    {


        if (!Capsule::schema()->hasTable('smsgateway_sms_template')) {
            Logger::logModule('error, schema for smsgateway_sms_temple does not exist');
            return;
        }

        if ((SmsTemplate::where('name', $hookName)->get()->count()) > 1) {
            Logger::logModule('info, elements for templates already exists');
            return;
        }



        $languages = array(
            'arabic', 'azerbaijani', 'catalan', 'chinese', 'croatian',
            'czech', 'danish', 'dutch', 'english', 'estonian', 'farsi',
            'french', 'german', 'hebrew', 'hungarian', 'italian', 'macedonian', 'norwegian',
            'polish', 'portuguese', 'romanian', 'russian', 'spanish', 'swedish',
            'turkish', 'ukranian'
        );
        $languagesFromDB = Capsule::table('tblemailtemplates')->groupBy('language')->select('language')->get();

        $insertData = array();
        if (count($languagesFromDB) < 1) {
            foreach ($languagesFromDB as $lang) {
                array_push($insertData, array('name' => $hookName, 'lang' => $lang->language, 'created_at' => date('Y-m-d H:i:s')));
            }
        } else {
            foreach ($languages as $lang) {
                array_push($insertData, array('name' => $hookName, 'lang' => $lang, 'created_at' => date('Y-m-d H:i:s')));
            }
        }
        Capsule::table('smsgateway_sms_template')->insert($insertData);
    }

    private function firstInstallationAddon(array $hooksName): void{
        foreach($hooksName as $hook){

            $this->createTemplateSchemaForHook($hook);
        }
    }
}
