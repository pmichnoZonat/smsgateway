<?php

namespace WHMCS\Module\Addon\SmsGateway;

use WHMCS\Module\Addon\Setting;
use WHMCS\Module\Addon\SmsGateway\Models\SmsService;
use WHMCS\Module\Addon\SmsGateway\Models\SmsTemplate;
use WHMCS\Module\Addon\SmsGateway\Models\SmsStats;
use WHMCS\Module\Addon\SmsGateway\SmsTwilio;
use Illuminate\Database\Capsule\Manager as DB;
use MGModule\Multibrand\Models\Relation;
use MGModule\Multibrand\Models\Brand;
use MGModule\Multibrand\Models\Setting as MultiBrandSetting;

class Hooks
{

    // it holds names of hooks which will be run it
    private $namesHooks = array(
        'InvoiceCreated',
        'UserCreation'
    );


    public function getDefault(): string
    {
        return (string) Setting::where('module', 'smsgateway')->where('setting', 'Default')->pluck('value')->first();
    }

    public function getUserForInvoice($vars)
    {
        return DB::table('tblclients')->join('tblinvoices', function ($join) use ($vars) {
            $join->on('tblclients.id', '=', 'tblinvoices.userid')
                ->where('tblinvoices.id', '=', $vars['invoiceid']);
        })->first();
    }



    public  function smsInvoiceCreated($vars, $smarty)
    {

        $inoviceName = $this->namesHooks[0];
        $userInoviceJoiner = $this->getUserForInvoice($vars);

        if (!$userInoviceJoiner->language) {
            $userInoviceJoiner->language = 'english';
        }
        //relation id
        $id = Relation::where('relid', $userInoviceJoiner->id)->pluck('brand_id')->first();
        //brand name from id 
        $brandName = Brand::where('id', $id)->pluck('name')->first();
        //service for twilio from brand name
        $smsService = SmsService::where('brandname', $brandName)->first();
        //systemUrl for specific brand
        $systemUrl = MultiBrandSetting::where('brand_id', $id)->where('setting', 'systemUrl')->pluck('value')->first();

        //If sid for brand does not exist we need to use default
        if (!$smsService) {
            $smsService->sid = $this->getDefault();
        }

        //getting template for langauge as well for hook name 
        $template = SmsTemplate::where('name', $inoviceName)->where('lang', $userInoviceJoiner->language)->first();

        $smarty->assign('brand_name', $brandName);
        $smarty->assign('whmcs_url', $systemUrl);
        $smarty->assign('invoice_no', ((string) $vars['invoiceid']));
        $smarty->assign('invoice_due_date', $userInoviceJoiner->duedate);

        $content =  $smarty->fetch('string:' . $template->content);

        try {
          $messageResponse =  (new SmsTwilio)->sendMessageFromService($userInoviceJoiner->phonenumber, $content, $smsService->sid);
          Logger::logModule(print_r($messageResponse, true));
          Logger::logModule(print_r($messageResponse->status, true));
        } catch (\Exception $e) {
            Logger::logModule('error,on sending message. ' . $e->getMessage());
            exit();
        }


        try {
            $stat = new SmsStats;
            $stat->type = 'invoice';
            $stat->relid = $vars['invoiceid'];
            $stat->note  = $this->namesHooks[0];
            $stat->save();
        } catch (\Exception $e) {
            Logger::logModule('error, on saving SmsStats. ' . $e->getMessage());
        }
    }


    public function smsAccountCreation($vars, $smarty){


        //getting user details login and password
        $userLogin =  $vars['username'];
        $userPassword  = $vars['password'];
    

        //form which brand we would like to send
        $id = Relation::where('relid', $vars[3])->pluck('brand_id')->first();
        //brand name from id 
        $brandName = Brand::where('id', $id)->pluck('name')->first();
        //service for twilio from brand name
        $smsService = SmsService::where('brandname', $brandName)->first();
        //systemUrl for specific brand
        $systemUrl = MultiBrandSetting::where('brand_id', $id)->where('setting', 'systemUrl')->pluck('value')->first();


          //If sid for brand does not exist we need to use default
          if (!$smsService) {
            $smsService->sid = $this->getDefault();
        }

        //getting template for langauge as well for hook name 
        $template = SmsTemplate::where('name', $this->namesHooks[1])->where('lang', $vars[4])->first();

        $smarty->assign('brand_name', $brandName);
        $smarty->assign('whmcs_url', $systemUrl);
        $smarty->assign('user_login', $userLogin);

        $content =  $smarty->fetch('string:' . $template->content);

        try {
            (new SmsTwilio)->sendMessageFromService($vars[5], $content, $smsService->sid);
        } catch (\Exception $e) {
            Logger::logModule('error,on sending message. ' . $e->getMessage());
            exit();
        }


        try {
            $stat = new SmsStats;
            $stat->type = 'invoice';
            $stat->relid = $vars['invoiceid'];
            $stat->note  = $this->namesHooks[0];
            $stat->save();
        } catch (\Exception $e) {
            Logger::logModule('error, on saving SmsStats. ' . $e->getMessage());
        }

    }
}
