<?php

namespace WHMCS\Module\Addon\SmsGateway;


class Logger
{
    const MODULE_NAME = 'SmsGateway Module';




    public static function logModule(string $message, int  $userId = 0): void
    {
        $string =  Logger::MODULE_NAME . ' ';

        $string .= $message;


        if ($userId > 1) {
            logActivity($string, $userId);
        } else {
            logActivity($string);
        }
    }
}
