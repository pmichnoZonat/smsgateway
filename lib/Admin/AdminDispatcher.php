<?php

namespace WHMCS\Module\Addon\SmsGateway\Admin;

use WHMCS\Module\Addon\SmsGateway\Admin\AdminController;
use WHMCS\Module\Addon\SmsGateway\Admin\TwilioController;

/**
 * Sample Admin Area Dispatch Handler
 */
class AdminDispatcher
{

    /**
     * Dispatch request.
     *
     * @param string $action
     * @param array $parameters
     *
     * @return string
     */
    public function dispatch($controller, $action, $parameters, $smarty)
    {
        if (!$action) {
            // Default to index if no action specified
            $action = 'index';
        }

        if (!$controller) {
            $controller = 'admin';
        }
        $controllerFullName = "WHMCS\Module\Addon\SmsGateway\Admin\\" . (ucfirst($controller) . 'Controller');
        if (!class_exists($controllerFullName, true)) {
            $controllerFullName = 'WHMCS\Module\Addon\SmsGateway\Admin\\AdminController';
        }
        $controllerDispatch = new $controllerFullName($smarty, $parameters['modulelink'], $controller);
        // Verify requested action is valid and callable
        if (is_callable(array($controllerDispatch, $action))) {
            return $controllerDispatch->$action($parameters);
        }
        
        return '<p>Invalid action requested. Please go <a href="'.$parameters['modulelink'].'" >back </a> and try again.</p>';
    }
}
