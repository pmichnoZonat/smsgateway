<?php

namespace WHMCS\Module\Addon\SmsGateway\Admin;

use WHMCS\Module\Addon\SmsGateway\SmsTwilio;
use WHMCS\Module\Addon\SmsGateway\Models\SmsService;
use MGModule\Multibrand\Models\Brand;

class TwilioController extends Controller
{






    public function createNewServiceTwilio($vars)
    {



        $smartyAssign =  array(
            'urls' => array(
                'back' => $this->url('index', 'admin'),
                'addNewService' => $this->url('createNewServiceTwilio'),
            ),
        );





        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $brandName = $_POST['brandname'];
            $sid = $_POST['sid'];

            if (empty($brandName) && empty($sid) || (empty($brandName) || empty($sid))) {
                $this->redirectWithParam('index', 'message', 'Empty Fields! One of the field was empty. (Adding new service to the addon.)', 'admin');
            }

            $brand = Brand::find($brandName);
            if (!$brand) {
                $this->redirectWithParam('index', 'message', 'Invalid Brand Name!', 'admin');
            }

            try {
                $twilio =  (new SmsTwilio)->fetchServiceForBrand($sid);
            } catch (\Exception $e) {
                Logger::logModule('error, message: '. $e->getMessage());
                $this->redirectWithParam('index', 'message', 'The message service of this ' . $sid . 'does not exist!', 'admin');
            }

            $service = new \WHMCS\Module\Addon\SmsGateway\Models\SmsService;
            $service->brandName = $brand->name;
            $service->sid = $twilio;

            $service->save();
            $this->redirectWithParam('index', 'message', 'Service was successfully added.', 'admin');
        } else {

            $brands = Brand::pluck('name', 'id')->toArray();
            $services = SmsService::pluck('brandName')->toArray();
            $smartyAssign['availableServices'] = array_diff($brands, $services);;
        }
        $this->smarty->assign('addNewService', $smartyAssign);
        return  $this->smarty->display($this->getPathToTheFile('twilioAddServiceTemplate'));
    }

    public function editServiceTwilio($vars)
    {

        $smartyAssign =  array(
            'urls' => array(
                'back' => $this->url('index', 'admin'),
                'editService' => $this->url('editServiceTwilio'),
            ),
        );


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $brandName = $_POST['brandname'];
            $sid = $_POST['sid'];

            if (empty($brandName) && empty($sid) || (empty($brandName) || empty($sid))) {
                $this->redirectWithParam('index', 'message', 'Empty Fields! One of the field was empty. (Editing  service, please select ' . $brandName . ' service again.)', 'admin');
            }
            $service = SmsService::where('brandname', $brandName)->first();
            if (!$service instanceof SmsService) {
                return $this->redirectWithParam('index', 'message', 'Service can\'t be edited. Please try again.', 'admin');
            }
            $service->sid = $sid;
            $service->brandname = $brandName;
            $service->save();
            
            $this->redirectWithParam('index', 'message', 'Service ' . $brandName . 'was successfully edited.', 'admin');
        } else {
            $id = $_GET['id'];

            $service = SmsService::find($id);


            if (!$service instanceof SmsService) {
                return $this->redirectWithParam('index', 'message', 'Service can\'t be edited. Please try again.', 'admin');
            }


            $smartyAssign['editableService'] = $service;
        }

        $this->smarty->assign('edit', $smartyAssign);

        return  $this->smarty->display($this->getPathToTheFile('twilioEditServiceTemplate'));
    }

    public function deleteServiceTwilio($vars)
    {
        $id = $_GET['id'];

        $service = SmsService::find($id);
        if (!$service instanceof SmsService) {
            return   $this->redirectWithParam('index', 'message', 'Service can\'t be deleted. Please try again.', 'admin');
        }
        try {
            $service->delete();
        } catch (\Exception $e) {
            Logger::logModule('error, on deleting service, message error:  '. $e->getMessage());

            return $this->redirectWithParam('index', 'message', 'Error on deleting service. Please try again.', 'admin');
        }

        $this->redirectWithParam('index', 'message', 'Service was successfully deleted.', 'admin');
    }
}
