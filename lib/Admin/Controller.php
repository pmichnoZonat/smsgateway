<?php

namespace WHMCS\Module\Addon\SmsGateway\Admin;

use WHMCS\Module\Addon\SmsGateway\AddonConfiguration;

use WHMCS\Module\Addon\SmsGateway\SmsTwilio;
/**
 * Sample Admin Area Controller
 */


/*
 display view for the method form etc. After can be switched so the form stays as the same place as view
 */
class Controller
{
    //Smarty template variable we setup it in constructor as passed value from up
    public $smarty;

    public $moduleLink;
    public $controllerName;
    public function __construct( $sm,  string $moduleLink, $controller="admin")
    {
       $this->setControllerName($controller);
        $this->setSmarty($sm);
        $this->setModuleLink($moduleLink);
    }

    public function setControllerName(string $name):void{
        $this->controllerName = $name;
    }
    
    //Getting the Smarty object, setting in in constructor,
    //because need it almost always in controller.
    public function setSmarty($sm): void
    {
        $this->smarty = $sm;
    }

    //$vars['modulelink'] is setup in constructor
    //for routing purposes so we don't call it everytime
    public function setModuleLink(string $moduleLink): void
    {
        $this->moduleLink = $moduleLink;
    }

    //Used for Smarty to get main directory of templates and
    //return ready path to the file as string with extension 
    // @params string $viewTemplate <- name of tamplate without .tpl
    public function getPathToTheFile(string $viewTemplate): string
    {
        return $this->smarty->getTemplateDir('main') . $viewTemplate  . '.tpl';
    }

    //It returns full path with modulelink
    // calling that function anywhere you get full
    // string path which can be used in template as href param
    //@params string @action   <- name of fucntion in Controller as action
    public function url( string $action, string $controller = 'default'): string
    {
        if($controller === 'default'){
            $controller = $this->controllerName;
        }    

        return  $this->moduleLink. '&controller='.$controller. '&action=' . $action;
    }



    public function redirectHomeAddon(string $controller = 'default'): void
    {
        if($controller === 'default'){
            $controller = $this->controllerName;
        }    

        header('Location: ' . $this->url('index', $controller));
        die();
    }

    public function redirectWithParam(string $route, string  $option, string $value, string $controller = 'default')
    {
        if($controller === 'default'){
            $controller = $this->controllerName;
        }    


        header('Location: ' . $this->url($route, $controller) . '&' . $option . '=' . $value);
        die();
    }

    /**
     * Display data with link for paginate such as previous and next
     * @param int $pageNumber  - current page starts from 0
     * @param string $actionMethod -  method name for example index 
     * @param string $functionNameToGetData - name of the function to call from current object 
     * !!! Warning take care of the context scope it might retrun undefined when call from wrong scope
     * @param string $storeName - subname in array for holding the data
     * 
     * @return array with paginate(prev, next) and data 
     */

    public function paginator(int $pageNumber,string $actionMethod, string $functionNameToGetData, string $storeName) {
        
        $smartyAssign = array();
        $currentPage = 1;
        if ($pageNumber) {
            if (!is_numeric($pageNumber)) {
                $pageNumber = 0;
            }
            if ($pageNumber)
                if ($pageNumber < 0) {
                    $pageNumber = 0;
                }

            $howmanyData = $pageNumber * 5;
            $prev = $pageNumber - 1;
            $next = $pageNumber + 1;
            $smartyAssign['paginate'] = array(
                'prev' => $this->url($actionMethod) . '&page=' . ($prev),
                'next' => $this->url($actionMethod) . '&page=' . ($next)
            );
            $data = call_user_func_array(array($this, $functionNameToGetData), array($howmanyData));
            if ($prev < 0) {
                $smartyAssign['paginate']['prev']    = null;
            }
            if (count($data ) < 5) {
                $smartyAssign['paginate']['next'] = null;
            }
            if (count($data ) <= 0) {
                $this->redirectHomeAddon();
            }
            $smartyAssign[$storeName] = $data ;
        } else {
            $data = call_user_func_array(array($this, $functionNameToGetData), array(0));
            if (count($data) > 5) {
                $smartyAssign['paginate'] = array('next' => $this->url($actionMethod) . '&page=' . ($currentPage));
            } else {
                $smartyAssign['paginate']['next'] = null;
            }
            $smartyAssign[$storeName] = $data;
        }
        \WHMCS\Module\Addon\SmsGateway\Logger::logModule(print_r($data,true));
           
        return $smartyAssign;
    }



    // it takes $_GET['message'] for the function
    public function msgValidation($message): array
    {
        return (!empty($message) ? array('valid' => true, 'msg' => $message) : array('valid' => false, 'msg' => ''));
    }


}
