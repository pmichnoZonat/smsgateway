<?php

namespace WHMCS\Module\Addon\SmsGateway\Admin;

use WHMCS\Module\Addon\SmsGateway\Models\SmsTemplate;
use WHMCS\Module\Addon\SmsGateway\Models\SmsStats;
use WHMCS\Module\Addon\SmsGateway\SmsTwilio;
use WHMCS\Module\Addon\SmsGateway\Models\SmsService;
use WHMCS\Module\Addon\SmsGateway\Logger;

class AdminController extends Controller
{




    public function getTemplates()
    {

        return    SmsTemplate::groupBy('name')->get();
    }

    public function getStats(int $value)
    {
        return SmsStats::skip($value)->take(15)->get();
    }

    public function editTemplateUrl(): string
    {
        return $this->url('editTemplate') . '&edit=';
    }
    public function defualtServiceUrl(): string
    {
        return $this->url('editDefaultService', 'twilio') . '&sid=';
    }
    public function addServiceToBrand(): string
    {
        return $this->url('createNewServiceTwilio', 'twilio');
    }

    public function editServiceToBrand(): string
    {
        return $this->url('editServiceTwilio', 'twilio') . '&id=';
    }

    public function deleteServiceBrand(): string
    {
        return $this->url('deleteServiceTwilio', 'twilio') . '&id=';
    }
    /**
     * 
     * Index action.
     *
     * @param array $vars Module configuration parameters
     *
     * @return string
     */
    public function index($vars)
    {

        $smartyAssign =  array(
            'urls' => array(
                'editTemplate' => $this->editTemplateUrl(),
                'addNewService' => $this->addServiceToBrand(),
                'editService' => $this->editServiceToBrand(),
                'deleteService' => $this->deleteServiceBrand(),
                'stats' => $this->url('stats'),
            ),
        );
        $msg = $this->msgValidation($_GET['message']);
        if ($msg['valid']) {
            $smartyAssign['message'] = $msg['msg'];
        }
        $smartyAssign['missingConfiguration'] = false;
        if (empty($vars['AuthToken'])) {

            $smartyAssign['missingConfiguration'] = true;
        }
        if (empty($vars['AccountSid'])) {

            $smartyAssign['missingConfiguration'] = true;
        }

        if (empty($vars['Default'])) {
            $smartyAssign['twilioDefault'] = false;
            $smartyAssign['missingConfiguration'] = true;
        }



        if (!$smartyAssign['missingConfiguration']) {
            $twilio = new SmsTwilio;
            if (!$twilio->verification()) {
                $smartyAssign['missingConfiguration'] = true;
            }
            try {
                $verifyDefaultService =  $twilio->fetchServiceForBrand($vars['Default']);
                $smartyAssign['twilioDefault'] = $verifyDefaultService;
                $smartyAssign['twilioDefaultNumbers'] = $twilio->fetchPhoneNumbersforService($verifyDefaultService, 20);
            } catch (\Exception $e) {
                $smartyAssign['twilioDefault'] = false;
            }

            $smsServices = SmsService::get()->toArray();

            if (count($smsServices) > 0) {
                $smsServices = array_map(function ($service) use ($twilio) {
                    try {
                        $service['phoneNumbers'] =  $twilio->fetchPhoneNumbersforService($service['sid'], 20);
                        return $service;
                    } catch (\Exception $e) {
                        Logger::logModule('error, message: ' . $e->getMessage());
                        return '';
                    }
                }, $smsServices);
                $smartyAssign['twilioServices'] =   $smsServices;
            }
        }







        $smartyAssign['smsTemplates'] = $this->getTemplates();

        $this->smarty->assign('index', $smartyAssign);
        return  $this->smarty->display($this->getPathToTheFile('index'));
    }









    public function editTemplate($vars)
    {

        $name = $_GET['edit'];
        if (!$name) {
            return $this->redirectHomeAddon();
        }

        $ediTemplate = SmsTemplate::where('name', $name)->get();

        $this->smarty->assign(
            'edit',
            array(
                'index' => $this->url('index'),
                'templateName' => $name,
                'editMethod' => $this->editTemplateUrl() . $name,
                'itemEdit' => $ediTemplate
            )
        );
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $hookName = $_POST['hookName'];
            $languages = $_POST['langTable'];


            foreach ($languages as $key => $lang) {
                try {
                    SmsTemplate::where('name', $hookName)->where('lang', $key)->update(['content' => $lang]);
                } catch (\Exception $e) {
                    Logger::logModule('error, message: ' . $e->getMessage());
                }
            }
            Logger::logModule('Templates for: ' . $hookName . ' updated without errors.');

            $this->redirectHomeAddon();
        }
        return  $this->smarty->display($this->getPathToTheFile('editTemplate'));
    }



    public function stats($vars)
    {
        $smartyAssign =  array(
            'urls' => array(
                'index' => $this->url('index'),

            )
        );

        $msg = $this->msgValidation($_GET['message']);
        if ($msg['valid']) {
            $smartyAssign['message'] = $msg['msg'];
        }
        $this->smarty->assign('indexStats', array_merge($smartyAssign, $this->paginator((int) $_GET['page'], 'stats', 'getStats', 'smsStats')));
        return  $this->smarty->display($this->getPathToTheFile('statsTemplate'));
    }
}
