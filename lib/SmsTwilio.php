<?php

namespace WHMCS\Module\Addon\SmsGateway;

use WHMCS\Module\Addon\Setting;

require __DIR__ . '/Twilio/autoload.php';

use Twilio\Rest\Client;


class SmsTwilio
{

  public  $accountSID;
  public  $authToken;
  public $twilioMessengerServiceId;

  public function __construct()
  {
    $settings = $this->requestSettings();
    $this->accountSID  = $settings['AccountSid'];
    $this->authToken  = $settings['AuthToken'];


    $this->twilioServiceSmsProviderSID = $settings['TwilioServiceSID'];
  }

  public  function requestSettings(): array
  {
    $settings = Setting::select('setting', 'value')->where(['module' => 'smsgateway'])->get();
    $config = array();
    foreach ($settings as $setting) {
      $config[$setting['setting']] = $setting['value'];
    }
    return $config;
  }

  private function clientTwilio()
  {
    if((!$this->accountSID) || (!$this->authToken)){
      Logger::logModule('Sid or AuthToken are not setup in configuration. ');
    throw new \Exception('Sid or AuthToken are not setup in configuration.');
    }

    try {
      return  new Client($this->accountSID, $this->authToken);
    } catch (\Exception $e) {
      Logger::logModule('error, Twilio Sms gateway module. '. $e->getMessage());
    }
  }

  public function verification()
  {
    try {
      $this->clientTwilio()->messaging->v1->services
        ->read(1);
      return true;
    } catch (\Exception $e) {
      Logger::logModule('error, Twilio Sms gateway module. '. $e->getMessage());
    
      return false;
    }
  }



  public function generateServiceForBrand(string $brandName): string
  {
    $twilioClient = $this->clientTwilio();
    $serviceCreation = $twilioClient->messaging->v1->services->create($brandName);
    return $serviceCreation->sid;
  }

  public function fetchServiceForBrand(string $serviceSid):string
  {
    $twilioClient = $this->clientTwilio();
    $service = $twilioClient->messaging->v1->services($serviceSid)->fetch();
    return $service->sid;
  }

 public function deleteService(string $serviceSid)
{
  $twilioClient = $this->clientTwilio();
  $service = $twilioClient->messaging->v1->services($serviceSid)->delete();

}

public function fetchPhoneNumbersforService(string $sid, int $howManyNumbers):array{
  $twilioClient = $this->clientTwilio();
  return  $twilioClient->messaging->v1->services($sid)->phoneNumbers
  ->read($howManyNumbers);
}

  public function sendMessageFromService($phoneNubmerToSend, $message, $twilioService)
  {
    $twilioClient = $this->clientTwilio();
    $messageResponse = $twilioClient->messages->create(
      // Where to send a text message (your cell phone?)
      $phoneNubmerToSend,
      array(
        'messagingServiceSid' =>$twilioService,
        'body' => $message

      )
    );
    return $messageResponse;
  }
}
