<?php


use WHMCS\Module\Addon\SmsGateway\AddonModule;




if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}




function smsgateway_config()
{
    $config = new AddonModule();
    return $config->getConfig();
}


function smsgateway_activate()
{
    $config = new AddonModule();

    return $config->activeAddon();
}

function smsgateway_deactivate()
{
   
}

function smsgateway_upgrade($vars)
{

    logActivity('call of upgrade sms gateway');
    $config = new AddonModule();

    $config->upgradeAddon($vars);
}

function smsgateway_output($vars)
{
   $smarty = new Smarty();
    $config =  new AddonModule;
    $smarty->setTemplateDir(array('main' => dirname(__FILE__) . '/templates'));
    $smarty->compile_dir = $GLOBALS['templates_compiledir'];
    echo $config->output($vars, $smarty);
}
