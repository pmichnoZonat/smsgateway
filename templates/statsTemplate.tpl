
<h1>Sms Stats</h1>

<br>
<br>
<a href="{$indexStats.urls.index}">Back </a>
<br>


<style type="text/css">
{literal}
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0lax{text-align:left;vertical-align:top}
{/literal}
</style>
{if $indexStats.smsStats}
<table class="tg">
<thead>
  <tr>

    <th class="tg-0lax">Type</th>
    <th class="tg-0lax">RelID</th>
    <th class="tg-0lax">Note</th>

  </tr>
</thead>
<tbody>
  {foreach $indexStats.smsStats as $stat}
  <tr>

    <td class="tg-0lax">{$stat->type}</td>
    <td class="tg-0lax">{$stat->relid}</td>
    <td class="tg-0lax">{$stat->note}</td>


  </tr>
    {/foreach}
</tbody>
</table>
{if $indexStats.paginate.prev}
<a href="{$indexStats.paginate.prev}">Prev</a>
{/if}

{if $indexStats.paginate.next}
<a href="{$indexStats.paginate.next}">Next</a>
{/if}

{else}
<p> No items <b>stats</b>.
{/if}