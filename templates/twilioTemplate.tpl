<style type="text/css">
{literal}
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0lax{text-align:left;vertical-align:top}
{/literal}
</style>


<h1>Twilio Manger</h1>

<a href="{$index.urls.homePage}">Home</a>
<ul>
<li><a href="{$index.urls.addNewService}">Add new service for brand</a></li>
</ul>

<a href="{$index.urls.editService}">Edit</a>
<a href="{$index.urls.removeService}">Delete</a>



<table class="tg">
<thead>
  <tr>
    
    <th class="tg-0lax">Name</th>
    <th class="tg-0lax">Sid</th>
    <th class="tg-0lax">Alphabetic Name</th>
    <th class="tg-0lax">Created</th>
    <th class="tg-0lax">Updated</th>
    <th class="tg-0lax">Display Numbers</th>
    <th class="tg-0lax">Edit</th>
    <th class="tg-0lax">Delete</th>

    </tr>
</thead>
<tbody>
  {foreach $index.messengerServices as $service}
  
  <tr>
    <td class="tg-0lax"> {$service->name}</td>
    <td class="tg-0lax"> {$service->sid}</td>
    <td class="tg-0lax"> {$service->alphabeticName}</td>
    <td class="tg-0lax"> {$service->created->date|date_format : "%D %H:%M:%S"}</td>
    <td class="tg-0lax"> {$service->updated->date|date_format : "%D %H:%M:%S"}</td>
    <td class="tg-0lax"> <a href="{$index.urls.displayList}&service={$service->sid}">Display</a></td>
    <td class="tg-0lax"> <a hre="{$index.urls.editService}{$service->sid}">Edit</a></td>
    <td class="tg-0lax"> <a href="{$index.urls.removeService}{$service->sid}">Delete</a></td>
  </tr>

    {/foreach}
</tbody>
</table>