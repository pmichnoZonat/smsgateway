 <h1> SMS Templates: {$edit.templateName}</h1>

<form action="{$edit.editMethod}" method="post">
   <input hidden name="hookName" value="{$edit.templateName}"  />
   <fieldset name="languages">

   <div>
  <div>
  <select name="lang" id="selectTemplate">
     {foreach $edit.itemEdit as $item}
  {if $item->lang == "english"}
  <option selected value="{$item->lang}">{$item->lang}</option>
  {else}
    <option  value="{$item->lang}">{$item->lang}</option>
  {/if}
     {/foreach}
  </select>
  </div>
      {foreach $edit.itemEdit as $item}
    <div class="editElement" hidden id="editElement_{$item->lang}">
        <textarea  type="text" rows="8" cols="50" maxlength='151' name="langTable[{$item->lang}]"   >{$item->content}</textarea>
     
    </div>
        {/foreach}
    </div>
    <div class="editor"  id="editor">
        <textarea class="inputText" type="text" rows="8" cols="50" maxlength='151'    ></textarea>
        <span id="counterElement"></span>
    </div>
     </fieldset>
   <input type="submit" name="submit" value="SAVE" />
</form>
<br>
 <a href="{$edit.index}">Cancel</a>
<div>
<p>Available variables: </p>
<p>{literal} {$brand_name} {/literal} - brand name from which message will be send.</p>
<p>{literal} {$whmcs_url} {/literal} - url to the backend.</p>
<p>{literal} {$invoice_no} {/literal}* - invoice number, only in InvoiceCreated template. It won't work on any other templates</p>
<p>{literal} {$invoice_due_date} {/literal}* - invoice date due, only in InvoiceCreated template. It won't work on any other templates</p>
</div>


 {literal}
<script type="text/javascript">

(function () {
   const maximumlength = 145;
   const selectableItems = document.getElementById('selectTemplate');
   const counterDispaly = document.getElementById('counterElement');
   const editor = document.getElementById('editor').querySelector('textarea');
   const searchQuery = 'editElement_';
   editor.innerHTML = document.
   getElementById(searchQuery+selectableItems.options[selectableItems.selectedIndex].value).
   querySelector('textarea').innerHTML;

   counterDispaly.innerHTML = (maximumlength - editor.value.length);

   editor.addEventListener('keyup', function(e){
    
const currentElementEdited =  document.
   getElementById((searchQuery+selectableItems.options[selectableItems.selectedIndex].value)).
   querySelector('textarea');
   if(e.keyCode == 8){
       counterDispaly.innerHTML = (maximumlength - editor.value.length);
      currentElementEdited.innerHTML = editor.value;
   }
  
   if((maximumlength - editor.value.length) <= 0){
      alert('Please use only maximum length of 145 otherwise sms will be send in two parts and content of sms will be not save it.');
      return;
   }else{
      counterDispaly.innerHTML = (maximumlength - editor.value.length);
      currentElementEdited.innerHTML = editor.value;
   }
},false)


selectableItems.addEventListener('change', function(e){

const toEdit =document.getElementById(searchQuery+e.target.value);
console.log(toEdit);
console.log(editor);
editor.value = toEdit.querySelector('textarea').value;
counterDispaly.innerHTML = (maximumlength - editor.value.length);
});
})();

</script>
{/literal}

<style type="text/css">
{literal}

{/literal}
</style>