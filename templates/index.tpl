<style type="text/css">
{literal}
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0lax{text-align:left;vertical-align:top}
{/literal}
</style>


{if $index.message}
{$index.message}
{/if}

{if $index.missingConfiguration}
<div class="alert alert-danger">
<p>Missing configuration </p>
<p>To configure go to: <b>Setup</b> -> <b>Addon Modules</b> -> <b> Sms Gateway (Configure)</b></p>
<p>Please setup correct <b>Account Sid</b> and <b>AuthToken</b></p>
{if !$index.twilioDefault}
<p><b>Default Service Sid</b> is required.</p>
{/if}

</div>
{else}

{if $index.twilioDefault}
<h2>Default Twilio service details</h2>

<table class="tg">
<thead>
  <tr>
    <th class="tg-0lax">Sid</th>
    <th class="tg-0lax">Phone Numbers</th>
     </tr>
</thead>
<tbody>

    <td class="tg-0lax">{$index.twilioDefault}</td>
    <td class="tg-0lax">
    {foreach $index.twilioDefaultNumbers as $number}
      <div>
  <p>{$number->phoneNumber}, {$number->countryCode}</p>
      <div>
      {/foreach}
    </td>
  </tr>

</tbody>
</table>
{else}
<h2>Default service is not yet setup, chose one of the service(sid) from Twilio console.</h2>
<p> please click here -> <a href="{$index.urls.twilioDefaultEdit}">Edit</a> </p>
{/if}


{if $index.twilioServices}
<h2>Twilio service configuration for specific brands</h2>
<a href="{$index.urls.addNewService}">Add</a>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0lax">Brand Name</th>
    <th class="tg-0lax">SID</th>
    <th class="tg-0lax">Phone Numbers</th>
    <th class="tg-0lax">Actions</th>
  </tr>
</thead>
<tbody>
  {foreach $index.twilioServices as $twilioService}
  <tr>
    <td class="tg-0lax">{$twilioService['brandname']}</td>
    <td class="tg-0lax">{$twilioService['sid']}</td>
    <td class="tg-0lax">
    {foreach $twilioService.phoneNumbers as $phoneNumber}
    <div>
  <p>{$phoneNumber->phoneNumber}, {$phoneNumber->countryCode}</p>
      <div>
      {/foreach}
    </td>
    <td class="tg-0lax"><a href="{$index.urls.editService}{$twilioService['id']}">Edit</a> | <a href="{$index.urls.deleteService}{$twilioService['id']}">Delete</a></td>
  </tr>
    {/foreach}
</tbody>
</table>
{else}
<p>No <b>services</b> for <b>brands</b>, please configure addon, adding new <b>service</b> -> <a href="{$index.urls.addNewService}">Add</a> </p>

{/if}




<br>
<p>
<a href={$index.urls.stats}>Display list of Stats</a>
</p>


{/if}

<h2> Templates</h2>
<div>


<table class="tg">
<thead>
  <tr>
    <th class="tg-0lax">Name</th>
    <th class="tg-0lax">Action</th>


  </tr>
</thead>
<tbody>
  {foreach $index.smsTemplates as $template}
  <tr>
    <td class="tg-0lax">{$template->name}</td>
    <td class="tg-0lax"><a href="{$index.urls.editTemplate}{$template->name}">Edit</a></td>
  </tr>
    {/foreach}
</tbody>
</table>
</div>