<?php

namespace WHMCS\Module\Addon\ExampleAddon\Tests;

use PHPUnit\Framework\TestCase;

use  WHMCS\Module\Addon\ExampleAddon\AddonConfiguration;
use WHMCS\Module\Addon\ExampleAddon\Fields;

class ExampleTest extends TestCase
{

    protected   $moduleName = 'exampleaddon';

    public function testOne()
    {

        $this->assertTrue(true);
    }


    public function testShouldBeArray()
    {
        $config =  new AddonConfiguration();
        $this->assertIsArray($config->getConfig());
    }


    public function testCheckFieldCreation()
    {
        $config =  new AddonConfiguration();


        //key 'name' value addonNmae  | all strings

        // make sure it returns bool

        $this->assertTrue($config->addOption('name', 'ExampleAddon'));
            $result = $config->getConfig();
        $this->assertArrayHasKey('name', $result);
        
    }

    
    public function testAddInvalidOption(){
        $config = new AddonConfiguration();
       
            $this->assertFalse( $config->addOption('HelloWorld', 'Addon Module Sample'));

    }

    public function testValidFields()
    {
        $fieldsTest = ["name", "description", "author", "language", "version", "fields"];

        $config = new AddonConfiguration();

        $config->addOption('name', 'Addon Module Sample');
        $config->addOption('description', 'This module provides an example WHMCS Addon Module' . ' which can be used as a basis for building a custom addon module.');
        $config->addOption('author', 'Sample   Spaceses');
        $config->addOption('language', 'english');
        $config->addOption('version', '1.0');

        //Create New Class Field which will generate array
        $fields = new Fields();
        $fields->generateFieldTypeText("Title", "Something","85","default value","descirpiton");
      
        $config->addFields($fields->getAllFields());

            $testOptions = $config->getConfig();
            
        $this->assertArrayHasKey('name',  $testOptions);
        $this->assertArrayHasKey('description',  $testOptions);
        $this->assertArrayHasKey('author',  $testOptions);
        $this->assertArrayHasKey('language',  $testOptions);
        $this->assertArrayHasKey('version',  $testOptions);
        $this->assertArrayHasKey('version',  $testOptions);


        
    }
    public function testIsResultHasAllElements(){
        $config = new AddonConfiguration();
        $config->addOption('name', 'Addon Module Sample');
        $config->addOption('description', 'This module provides an example WHMCS Addon Module' . ' which can be used as a basis for building a custom addon module.');
        $config->addOption('author', 'Sample   Spaceses');
        $config->addOption('language', 'english');
        $config->addOption('version', '1.0');

        //Create New Class Field which will generate array

      
        $config->addFields( array());

        $countNumberOfFields = 6;
        // func which will check if we have all options
        $this->assertCount($countNumberOfFields, $config->getConfig(), "Result has not enough elements!");

    }


    public function testFunctionAddonActive(){

      
        $this->assertArrayHasKey('status',call_user_func($this->moduleName . '_activate'));
    }

}
