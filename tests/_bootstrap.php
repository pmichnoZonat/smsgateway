<?php
if (!defined('WHMCS')) {
    define('WHMCS', true);
}


require_once __DIR__ . '/../exampleaddon.php';
require_once __DIR__ . '/../lib/Fields.php';
require_once __DIR__ . '/../lib/AddonConfiguration.php';
require_once __DIR__ . '/../lib/Models/Model.php';
require_once __DIR__ . '/../lib/Models/SmsStats.php';
require_once __DIR__ . '/../lib/Models/SmsTemplate.php';