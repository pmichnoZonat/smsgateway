<?php

namespace WHMCS\Module\Addon\ExampleAddon\Tests;

use PHPUnit\Framework\TestCase;

use WHMCS\Module\Addon\ExampleAddon\Models\SmsTemplate;


class SmsTemplateTest extends TestCase
{
    public function testGenerateDatabase(){
        $db = new SmsTemplate('addonexample_sms_template');
        
        
        $this->assertIsArray($db->generateDatabaseSchema());
    }

}