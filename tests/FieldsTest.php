<?php

namespace WHMCS\Module\Addon\ExampleAddon\Tests;

use PHPUnit\Framework\TestCase;
use WHMCS\Module\Addon\ExampleAddon\Helper\Fields;


class FieldTest extends TestCase
{


    public function testFieldsisNotEmpty()
    {
        $fields = new Fields();
        //title, type, value      
        $fields->generateFieldTypeText("Title", "Something","85","default value","descirpiton");
        $this->assertCount(1, $fields->getAllFields());
    }

    public function testResultIsArray(){
        $fields = new Fields();
        //title, type, value      
        $fields->generateFieldTypeText("Title", "Something","85","default value","descirpiton");
        $this->assertIsArray($fields->getAllFields());

    }

  
}
